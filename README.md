# Lab8 - measure failure intensity

## Result
**MTTF** = 405886 ms / 19437 = **20,8821320163** ms <br/>
**FI** = 1 / MTTF = **0,04788783057** <br/>
![Result](./result.png)


## Introduction

Ok, during the last lab we were doing load testing, and this is a good tool to understand what load you can withstand. But how to understand if you need to to quality maitenance, and call an ambulance to save your product. There is the only way to understand it, you need to measure failure intensity of your product, and guess what we are going to do today. ***Let's roll!***

## Failure intensity

Well, this is a time when the name speaks for itself. Failure intensity is an amount of failures on your service during the unit of time(or code). Measuring it will help you to understand if something wrong goes with your service, to check it you may count number of 503/404 and other responses, compared to your normal 200s.It is not necessary to check this number on a prod. as you can use tools discussed on the previous lab to measure it.

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab8 - Failure intensity
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab8-measure-failure-intensity)
2. Ok, as well as previous time, open this [link](https://docs.google.com/spreadsheets/d/1lLigF6um8pKH2IGZNt9xXcl8quPtFPzmVtt5RCLeCqw/edit?usp=sharing) and paste url somewhere, as we're going to use it.
3. To test it we are going to use the same thing that we've used previous time (Apache Benchmark), you should run command like this one:
```sh
ab -n 20000 -c 100 -m "GET" _url_
```
This means that we are going to send 20000 requests, with 100 of them sending concurrently to the server. We have few lines like `Failed requests` and `Non-2xx responses`, which are exactly failures we're searching here. To measure the exact metric you should use just this formule, where MTTF is time between failures:
`FI = 1 / MTTF`.

## Homework

As a homework you will need to open this [link](https://docs.google.com/spreadsheets/d/1lLigF6um8pKH2IGZNt9xXcl8quPtFPzmVtt5RCLeCqw/edit?usp=sharing), load test your service(GetSpec request) and provide the results of your test as a screenshot when failure intensity is equal to the `0.2+-5%`. + provide the calculations for the failure intensity
